# Real estate price prediction

-This purpose of this project is to compare and contrast results from TWO statistics models designed to predict housing prices of real estate assets in a specific area given predefined asset and environmental characteristics (modified data is used). Specifically, the effect of the “age” variable on the mean observed price per unit is predicted. The steps are explained in detail and the results are analysed before a particular machine learning model is chosen and recommended to predict house prices. The two models compared are the RANSAC regression and random forest models. 

-This project incorporates Python and statistics models used in machine learning. Python libraries such as numPy and Pandas are used to make use of data analysis and statistics. 
