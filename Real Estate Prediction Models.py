#********Section 1: Introduction & steps *****************
"""
    -Context: 
        Predicting the fair value of property accurately can be an important tool that sets oneself at an advantage in 
        the context of property trading/investing. Zillow, an American online real estate company, was recently also involved in the 
        business of buying and selling homes until it recently exited this area of its business. This was due to an inability to accurately 
        predict housing prices, which made it difficult to successfully operate in this area. What followed was a 25% decline in Zillow's share price, 
        losing two thirds of its value since February 2021.
    
    -Objective: 
        The objective of this project is to predict housing prices of real estate assets in a specific area given certain characterstics.
        My FinTech members are specifically interested to see the effect of the 'age' variable on the mean observed house price per unit. 
        Two models will be chosen from the 6 given and they will be compared based on their coefficicents and model accuracy by analysing their mean standard 
        error (MSE) and R^2. 
    
    -Information on dataset: 
        
        #ML package links:   http://scikit-learn.org/stable/index.html
        Data Source:        https://www.kaggle.com/quantbruce/real-estate-price-prediction?select=Real+estate.csv
        Attributes:
                1. No -  The number of the row in the data set
                2. trade_date -  The date of the transaction as a number rounded to 3 decimal places
                3. age -  The age of the house as a number rounded to 1 decimal 
                4. distance_to_MTR -  Distance to the nearest MRT station  
                5. number_of_stores -  Number of conveniance stores 
                6. latitude  -  latitude of the property 
                7. longtitude -  longtitude of the property 
                8. house_price_per_unit -  The price of the house per unit, rounded to the nearest 1 decimal place        
    
        DATA SAMPLE:   
        No trade_date age distance_to_MTR number_of_stores	latitude longtitude house_price_per_unit	
        1	2012.917  32    84.87882  10 24.98298  121.54024	37.9	
        2	2012.917  19.5  306.5947  9	 24.98034  121.53951	42.2	
        3	2013.583  13.3  561.9845  5  24.98746  121.54391	47.3
        4	2013.5    13.3  561.9845  5	 24.98746  121.54391	54.8
        5	2012.833  5	    390.5684  5  24.97937  121.54245	43.1	
    
    -Models used:
        The two machine learning models that will be used and compared are the RANSAC regression and random forest models. 
        
        RANSAC (RANdom SAmple Consensus) algorithm takes linear regression to the next level by excluding 
        the outliers in the training dataset. The presence of outliers in the training dataset does impact the 
        coefficients and the model that is created. Thus, it would be suitable to identify and remove the 
        outliers during the early phase. The presence of outliers, when 'age' is used as the independant variable with 
        the dependant variable as the price, are very apparent in the sample data set provided. Other factors may affect  
        the prices in the sample data causing the outliers to exist, while we are only concerned with direct relationship 
        that age has on the house price per unit. Hence, it is best to remove these outliers and focus on areas where there is a
        concentration of data. Overall, the RANSAC regression may be a great use for displaying a linear relationship between age and 
        house price per unit. 
        
        The random forest regression model was chosen to be compared with the RANSAC regression as it is a non-linear model that also has the potential 
        to be an accurate model. The random forest creates various randomly generated decision trees and calculates the average of all the decision trees to 
        generate a great estimate. A single decision tree is highly sensitive where slight changes in data can cause drasticallly diferrent trees. A decision 
        tree will incorpate outliers into the model and may create highly inacurrate models based on the data that was used. Hence, a random forest was used in an attempt to 
        display a more accurate prediction using the average. The random forest looks at all the attributes and their values to determine what value will be used as the root note 
        in the decision tree. The random forest will be based on the mean squared error (MSE) where the decision tree with the lowest MSE will be chosen. 
    
   
    -Steps (Methodology): 
        Initially, the data is loaded into the python file while the columns are defined. The data is stored as df
        and is printed to make sure the data is loaded correctly. 
        
        For each of the models used the following steps are done: 
            -Step 1: Create and fit model 
                The regression model used to predict the Y variable is created based on a function that atkes in paramaters to define how it will work. 
                The sample data X and Y variables are then used to fit the model. 
            
            -Step 2: Create a plot and display predicted relationship based on model
                A plot is created and displayed based on the X and Y variabes where is relatiosnhip is displayed as well as the raw data 
                
            -Step 3: Measure model in terms of predictions by comparing between train and test data
                The train and test data is split in a 7:3 ratio randomly in preparation for comparision. 
                The train data is used to fit the model 
                The test and train X variables are both used to predict the Y train and test variables based on the model fitted using the train data 
                Both the Y predicted train and test values are compared against the raw data of the Y train and test values to measure performance. 
                Performance is measured by calculating the mean standard error (MSE) and R^2 of both the train and test data. MSE shows the error in predicting the Y value while
                R^2 displays the percentage the model predicts the Y variable based on its input. 
                
            -Step 4: Create a plot for the predicted values vs residuals
                A plot is created and displayed showing the residuals, which is the difference between the actual data and predicted data, over the range of the 
                Y predicted values.     
    
"""

#***************** Section 2: Implementation *****************
#Import packages 
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression

# Load real estate data 
df = pd.read_csv("/Users/kennethbernard/Downloads/T3_2021_RE.csv")
df.columns = ['No', 'trade_date', 'age', 'distance_to_MTR',
              'Nnumber_of_stores', 'latitude', 'longitude', 'house_price_per_unit']

#Print sample data set 
print(df)

# define function for creating a random forest plot based on X & Y variables and model parameters
def rf_plot(X, y, model):
    plt.scatter(X, y, c='lightblue') 
    plt.plot(X, model.predict(X), color='red', linewidth=3) #X variable paramater; model.predict(X) creates the line based on the X variable predicting the Y values 
    return


# **** RANSACRegressor model ****

#Step 1: Create and fit model 
# information on ransacRegressor: http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RANSACRegressor.html
from sklearn.linear_model import RANSACRegressor
#Determine how the RANSAC model will be created using paramaters 
ransac = RANSACRegressor(LinearRegression(), #regression is based on a linear regression 
                         min_samples=289, #highest min_samples allowable in this context is 289 
                         residual_threshold=28.0, #A residual threshold decides the inliers and outliers - a 28 threshold was suitable for this sample data as it excluded data that was far away from the concentration of the data 
                         random_state=0) #The random state determines how data will be selected: A 0 random state will mean that no seed is used in order to make the random state as random as possible 

# Variables defined, in this case one X variable age vs house price per unit
X = df[['age']].values
y = df['house_price_per_unit'].values

#Fit RANSAC model to given training data and target values.
ransac.fit(X, y)

#Return the inlier and outlier values from the training data 
inlier_mask = ransac.inlier_mask_
outlier_mask = np.logical_not(inlier_mask)


#Step 2: Create a plot and display predicted relationship based on model 
line_X = np.arange(0, 50, 1) #Creates a range from 0 to 50 
line_y_ransac = ransac.predict(line_X[:, np.newaxis]) #Create a line showing the dependant variable Y, using the X variable range based on the model 
plt.scatter(X[inlier_mask], y[inlier_mask], c='blue', marker='o', label='Inliers') #Create a scatter plot of the inlier values 
plt.scatter(X[outlier_mask], y[outlier_mask], c='lightgreen', marker='s', label='Outliers') #Create a scatter plot of the outlier values 
plt.plot(line_X, line_y_ransac, color='red') #Create a line showing the relationship between the X variable range and the predicted Y values 
plt.xlabel('Average age of house [age]') #Create X label 
plt.ylabel('House price per unit [house_price_per_unit]') #Create Y label 
plt.legend(loc='upper left') #Create legend 

plt.tight_layout()
plt.show() #Output plot 

print('RANSAC Slope: %.3f' % ransac.estimator_.coef_[0]) #print the slope of the ransac model rounded to 3 decimal figures 
print('RANSAC Intercept: %.3f' % ransac.estimator_.intercept_) #print the intercept of the ransac model to 3 sdecimal figures 


#Step 3: Measure model in terms of predictions by comparing between train and test data 
from sklearn.model_selection import train_test_split

X = df.iloc[:, :-1].values #define X as being all values excluding the last column 
y = df['house_price_per_unit'].values #define y as being the house price per unit 

#Split the train and test data with a 7:3 ratio (test_size = 0.3) where data is chosen randomly with no seed (random state=0)
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=0)

#fit ransac model with train data 
ransac.fit(X_train, y_train)

#predict the Y train and test values using the X train and test values 
y_train_pred = ransac.predict(X_train)
y_test_pred = ransac.predict(X_test)

from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error

#Comparing MSE between train and test data 
print('Ransac MSE train: %.3f, test: %.3f' % (
        mean_squared_error(y_train, y_train_pred), #displays the error (MSE) the model has in predicting the train values 
        mean_squared_error(y_test, y_test_pred))) #displays the error (MSE) the model has in predicting the test values 

#Comparing R^2 between train and test data 
print('Ransac R^2 train: %.3f, test: %.3f' % (
        r2_score(y_train, y_train_pred), #R^2 of train data shows what percentage of the Y train variable is determined by the model 
        r2_score(y_test, y_test_pred))) #R^2 of test data shows what percentage of the Y test variable is determined by the model 

#Step 4: Create a plot for the predicted values vs residuals - include train and test data 
plt.scatter(y_train_pred,  y_train_pred - y_train, c='blue', marker='o', label='Training data') #scatter plot of residuals over the Y predicted values range where the residual is the difference between the y train values and y train predicted values 
plt.scatter(y_test_pred,  y_test_pred - y_test, c='lightgreen', marker='s', label='Test data') #scatter plot of residuals over the Y predicted values range where the residual is the difference between the y test values and y test predicted values 
plt.xlabel('RANSAC Predicted values')
plt.ylabel('RANSAC Residuals')
plt.legend(loc='upper left')
plt.hlines(y=0, xmin=-10, xmax=50, lw=2, color='red')
plt.xlim([0, 60]) #determines the X range of the plot 
plt.tight_layout()
plt.show()



#****Random Forrest model***
#Step 1: Create and fit model 
# http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html

from sklearn.ensemble import RandomForestRegressor

#create random forest model 
forest = RandomForestRegressor(n_estimators=1000, #uses 1,000 trees 
                               criterion='mse', #criteria is based on lowest mse of decision trees 
                               max_depth=3, #the max depth of 3 is suitabe for not overfitting the data especially with presence of outliers 
                               n_jobs=-1) #number of jobs running in parallel 

X = df[['age']].values
y = df['house_price_per_unit'].values

forest.fit(X, y)

sort_idx = X.flatten().argsort()

#Step 2: Create a plot and display predicted relationship based on model 
rf_plot(X[sort_idx], y[sort_idx], forest) #X & Y variables and model is passed into rf plot function 
plt.xlabel('Average age [age]')
plt.ylabel('House price per unit [house_price_per_unit]')
plt.show()

#Step 3: Measure model in terms of predictions by comparing between train and test data
X = df.iloc[:, :-1].values
y = df['house_price_per_unit'].values

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=0)
forest.fit(X_train, y_train)
y_train_pred = forest.predict(X_train)
y_test_pred = forest.predict(X_test)

print('RF MSE train: %.3f, test: %.3f' % (
        mean_squared_error(y_train, y_train_pred),
        mean_squared_error(y_test, y_test_pred)))
print('RF R^2 train: %.3f, test: %.3f' % (
        r2_score(y_train, y_train_pred),
        r2_score(y_test, y_test_pred)))

#Step 4: Create a plot for the predicted values vs residuals - include train and test data 
plt.scatter(y_train_pred,
            y_train_pred - y_train,
            c='black',
            marker='o',
            s=35,
            alpha=0.5,
            label='Training data')
plt.scatter(y_test_pred,
            y_test_pred - y_test,
            c='lightgreen',
            marker='s',
            s=35,
            alpha=0.7,
            label='Test data')

plt.xlabel('RF Predicted values')
plt.ylabel('RF Residuals')
plt.legend(loc='upper left')
plt.hlines(y=0, xmin=-10, xmax=50, lw=2, color='red')
plt.xlim([-10, 50])
plt.tight_layout()
plt.show()


# *****************Section 3: Analysis & results *****************
'''
Analysis & results:
    
    -The RANSAC model has a train MSE of 79.956 and a test MSE of 72.551. The RANSAC R^2 for train data is 0.580 
    while the R^2 for test data is 0.574.
    -The random forest model has a train MSE of 43.542 and a test MSE of 64.093. The random forest R^2 for train data is 0.771 
    while the R^2 for test data is 0.624
    -The random forest model used has the lowest test MSE here even though it is higher than its train MSE. The R^2 of the RANSAC train and 
    test are similar while the R^2 of the random forest vary more widely. 
    
Recommendation :
    -random forest is a more accurate predictor of the house price given the age variable
    as it simply has the lowest test MSE between the two models. 
    -Hence, the random forest model is better suited for the prediction of house prices per unit 
    given the age variable.
    
    -This model may be used to preict house prices for companies like Zillow. 
    
'''













